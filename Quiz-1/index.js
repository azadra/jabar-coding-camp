// Kuis Soal 1
// Function Penghasil Tanggal Hari Esok
function next_date(tanggal,bulan,tahun) {
    var besok;
    if (tahun >= 0 && tahun <= 10000) {
        switch (bulan) {
            case 1:
                if(tanggal === 31) {
                    besok = "Besok adalah " + 1 + " Februari " + tahun;
                }else if(tanggal >= 1 && tanggal <= 30){
                    besok = "Besok adalah " + (tanggal+1) + " Januari " + tahun;
                }else {
                    besok = "Sepertinya Anda salah Format";
                }
                break;
            case 2:
                if (tanggal === 28 && tahun % 4 !== 0) {
                    besok = "Besok adalah " + 1 + " Maret " + tahun;
                }else if (tanggal === 29 && tahun % 4 === 0) {
                    besok = "Besok adalah " + 1 + " Maret " + tahun;
                }else if(tanggal === 28 && tahun % 4 === 0) {
                    besok = "Besok adalah " + (tanggal+1) + " Februari " + tahun;
                }
                else if(tanggal >= 1 && tanggal <= 27){
                    besok = "Besok adalah " + (tanggal+1) + " Februari " + tahun;
                }else {
                    besok = "Sepertinya Anda salah Format";
                }
                break;            
            case 3:
                if(tanggal === 31) {
                    besok = "Besok adalah " + 1 + " April " + tahun;
                }else if(tanggal >= 1 && tanggal <= 30){
                    besok = "Besok adalah " + (tanggal+1) + " Maret " + tahun;
                }else {
                    besok = "Sepertinya Anda salah Format";
                }
                break;
            case 4:
                if(tanggal === 30) {
                    besok = "Besok adalah " + 1 + " Mei " + tahun;
                }else if(tanggal >= 1 && tanggal <= 29){
                    besok = "Besok adalah " + (tanggal+1) + " April " + tahun;
                }else {
                    besok = "Sepertinya Anda salah Format";
                }
                break;
            case 5:
                if(tanggal === 31) {
                    besok = "Besok adalah " + 1 + " Juni " + tahun;
                }else if(tanggal >= 1 && tanggal <= 30){
                    besok = "Besok adalah " + (tanggal+1) + " Mei " + tahun;
                }else {
                    besok = "Sepertinya Anda salah Format";
                }
                break;
            case 6:
                if(tanggal === 30) {
                    besok = "Besok adalah " + 1 + " Juli " + tahun;
                }else if(tanggal >= 1 && tanggal <= 29){
                    besok = "Besok adalah " + (tanggal+1) + " Juni " + tahun;
                }else {
                    besok = "Sepertinya Anda salah Format";
                }
                break;
            case 7:
                if(tanggal === 31) {
                    besok = "Besok adalah " + 1 + " Agustus " + tahun;
                }else if(tanggal >= 1 && tanggal <= 30){
                    besok = "Besok adalah " + (tanggal+1) + " Juli " + tahun;
                }else {
                    besok = "Sepertinya Anda salah Format";
                }
                break;                                                                   
            case 8:
                if(tanggal === 31) {
                    besok = "Besok adalah " + 1 + " September " + tahun;
                }else if(tanggal >= 1 && tanggal <= 30){
                    besok = "Besok adalah " + (tanggal+1) + " Agustus " + tahun;
                }else {
                    besok = "Sepertinya Anda salah Format";
                }
                break;
            case 9:
                if(tanggal === 30) {
                    besok = "Besok adalah " + 1 + " Oktober " + tahun;
                }else if(tanggal >= 1 && tanggal <= 29){
                    besok = "Besok adalah " + (tanggal+1) + " September " + tahun;
                }else {
                    besok = "Sepertinya Anda salah Format";
                }
                break;
            case 10:
                if(tanggal === 31) {
                    besok = "Besok adalah " + 1 + " November " + tahun;
                }else if(tanggal >= 1 && tanggal <= 30){
                    besok = "Besok adalah " + (tanggal+1) + " Oktober " + tahun;
                }else {
                    besok = "Sepertinya Anda salah Format";
                }
                break;
            case 11:
                if(tanggal === 30) {
                    besok = "Besok adalah " + 1 + " Desember " + tahun;
                }else if(tanggal >= 1 && tanggal <= 29){
                    besok = "Besok adalah " + (tanggal+1) + " November " + tahun;
                }else {
                    besok = "Sepertinya Anda salah Format";
                }
                break;
            case 12:
                if(tanggal === 31) {
                    besok = "Besok adalah " + 1 + " Januari " + (tahun+1);
                }else if(tanggal >= 1 && tanggal <= 30){
                    besok = "Besok adalah " + (tanggal+1) + " Desember " + tahun;
                }else {
                    besok = "Sepertinya Anda salah Format";
                }
                break;
            default:
                besok = "Sepertinya Anda salah Format";
                break;
        }
    }else {
        besok = "Sepertinya Anda salah Format";
    }
    return besok;
}
var tanggal = 29;
var bulan = 2;
var tahun = 2020;
console.log(next_date(tanggal , bulan , tahun ));

var tanggal = 28;
var bulan = 2;
var tahun = 2021;
console.log(next_date(tanggal , bulan , tahun ));

var tanggal = 31;
var bulan = 12;
var tahun = 2020;
console.log(next_date(tanggal , bulan , tahun ));

var tanggal = 05;
var bulan = 10;
var tahun = 2021;
console.log(next_date(tanggal , bulan , tahun ));




// Kuis Soal 2
// Function Penghitung Jumlah Kata
function jumlah_kata(string) {
    var kata = string.split(" ");
    var count = 0;
        kata.forEach(function(word) {
            if (word !== '') {
                count++;
            }
        });
    return count;
}
var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok ";
var kalimat_2 = "Saya Iqbal";
console.log(jumlah_kata(kalimat_1));
console.log(jumlah_kata(kalimat_2));
console.log(jumlah_kata(" Hai nama saya Muhammad Azam Gresa Mahendra"));
console.log(jumlah_kata("Hari ini adalah hari saya mengerjakan quiz JCC pertama "));
