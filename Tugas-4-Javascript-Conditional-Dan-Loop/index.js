// Soal 1
var nilai = 90;
if (nilai >= 80) {
    console.log("Indeksnya A");
}else if (nilai >= 75 && nilai <= 85) {
    console.log("Indeksnya B");
}else if (nilai >= 65 && nilai <= 75) {
    console.log("Indeksnya C");
}else if (nilai >= 55 && nilai <= 65) {
    console.log("Indeksnya D");
}else {
    console.log("Indeksnya E");
}


// Soal 2
var tanggal = 14;
var bulan = 5;
var tahun = 1998;

switch (bulan) {
    case 1:
        console.log(tanggal + " Januari " + tahun);
        break;
    case 2:
        console.log(tanggal + " Februari " + tahun);
        break;
    case 3:
        console.log(tanggal + " Maret " + tahun);
        break;
    case 4:
        console.log(tanggal + " April " + tahun);
        break;
    case 5:
        console.log(tanggal + " Mei " + tahun);
        break;
    case 6:
        console.log(tanggal + " Juni " + tahun);
        break;
    case 7:
        console.log(tanggal + " Juli " + tahun);
        break;
    case 8:
        console.log(tanggal + " Agustus " + tahun);
        break;
    case 9:
        console.log(tanggal + " September " + tahun);
        break;
    case 10:
        console.log(tanggal + " Oktober " + tahun);
        break;
    case 11:
        console.log(tanggal + " November " + tahun);
        break;
    case 12:
        console.log(tanggal + " Desember " + tahun);
        break;
    default:
        break;
}


// Soal 3
var n = 7;
var segitiga = "";
for (var i = 1; i <= n; i++) {
    for (var j = 0; j < i ;j++) {
        segitiga += "#";
    }
        segitiga += "\n";
}
console.log(segitiga);


// Soal 4
var m = 10;
console.log("Output m = " + m);
var batas = "===";
for (var i = 1; i <= m; i++) {
    if (i % 3 === 0) {
        console.log(i + " - I Love VueJS" + "\n" + batas);
        batas += "===";
    }else if (i % 3 === 2) {
        console.log(i + " - I love Javascript");
    }else{
        console.log(i + " - I love programming");
    }  
}