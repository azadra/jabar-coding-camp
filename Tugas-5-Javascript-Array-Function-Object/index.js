// Soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarHewan.sort();
for (var i = 0; i < 5 ;i++) {
    console.log(daftarHewan[i]);
}


// Soal 2
function introduce(name,age,address,hobby) {
    myName = "Nama saya " + name + ", ";
    myOld = "umur saya " + age + " tahun, ";
    myAddress = "alamat saya di " + address + ", ";
    myHobby = "dan saya punya hobby yaitu " + hobby;
    return myName + myOld + myAddress + myHobby;
}   
var data = {name: "M. Azam",age: 23, address: "Ujung Berung Bandung ", hobby: "Gaming"};
var perkenalan = introduce(data.name,data.age,data.address,data.hobby);
console.log(perkenalan);


// Soal 3
function hitung_huruf_vokal(vokal) {
    var arr = ["a", "i", "u", "e", "o"];
    var huruf = vokal.toLowerCase();
    var count = 0;
    for (var i = 0;  i < huruf.length; i++) {
            if (arr[0] === huruf[i] || arr[1] === huruf[i] || arr[2] === huruf[i] || arr[3] === huruf[i] || arr[4] === huruf[i]){
                count++;
            }
    }
    return count;
}
var hitung_1 = hitung_huruf_vokal("Muhammad");
var hitung_2 = hitung_huruf_vokal("Iqbal");
console.log(hitung_1 , hitung_2);


// Soal 4
function hitung(angka) {
    var jmlah = angka * 2 - 2; 

    return jmlah;
}
console.log(hitung(0));
console.log(hitung(1));
console.log(hitung(2));
console.log(hitung(3));
console.log(hitung(5));