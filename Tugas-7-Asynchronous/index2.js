var readBooksPromise = require('./promise.js')

var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]

// Lanjutkan code untuk menjalankan function readBooksPromise
const promise = readBooksPromise(10000, books[0]);
promise
    .then(sisatime => {
        return readBooksPromise(sisatime, books[1])
    .then(sisatime => {
        return readBooksPromise(sisatime, books[2])
    .then(sisatime => {
        return readBooksPromise(sisatime, books[3])

    .catch(sisatime => {
        return sisatime})
    })})
    .catch(sisatime => {
        return sisatime})
    })
    .catch(sisatime => {
        return sisatime})